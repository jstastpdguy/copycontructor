#include "matrix.cpp"
#include <iostream>
#include <string>

using namespace homework;

int main()
{
	int cols = 3;
	int rows = 3;
	
	std::string first_name("one");
	std::string second_name("two");
	
	matrix<int> one(&rows, &cols, &first_name, nullptr);

	cols++;
	rows++;

	matrix<int> two(one);
	matrix<int> three(&rows, &cols, &second_name, &one);
	matrix<int> four = three;
	
	std::cout << one << std::endl << two << std::endl << three << std::endl << four;

	four = three = two = one;

	std::cout << one << std::endl << two << std::endl << three << std::endl << four;

	return 0;
}