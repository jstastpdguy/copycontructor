#include "matrix.h"
#include <string>

namespace homework
{
	template <class T>
	matrix<T>::matrix(int * _cols, int * _rows, std::string * matrix_name, matrix<T> * _prev)
	{
		cols = new int(*_cols);
		rows = new int(*_rows);
		
		//memory alloc
		array = new T*[*cols];
		
		for(int i = 0; i < *cols; i++)
		{
			array[i] = new T[*rows];
		}

		//fill all elements of array with fill_value
		for(int i = 0; i < *rows; i++)
		{
			for(int j = 0; j < *cols; j++){
				array[i][j] = rand()%9 + 1;
			}
		}

		//alloc mem for name and init by value
		name = new std::string(*(matrix_name));

		prev = _prev;
	}

	template <class T>
	matrix<T>::matrix(matrix const& other)
	{
		cols = new int(*other.cols);
		rows = new int(*other.rows);

		array = new T*[*cols];
		
		for(int i = 0; i < *cols; i++)
		{
			array[i] = new T[*rows];
		}

		for(int i = 0; i < *rows; i++)
		{
			for(int j = 0; j < *cols; j++){
				array[i][j] = other.array[i][j];
			}
		}

		name = new std::string(*(other.name));
		prev = other.prev;
	}

	template <class T>
	matrix<T>::~matrix()
	{
		//print name of deleted matrix
		std::cout << *name << " deleted\n";

		//free memory from array rows
		for (int i = 0; i < *cols; i++)
		{
			delete [] array[i];
		}
		
		//free memory from array of pointers
		delete [] array;

		//free mem from string*
		delete name;

		//free int*
		delete cols;
		//free int*
		delete rows;

		//sets prev pointer to null
		prev = nullptr;
	}

	template <class T>
	matrix<T>& matrix<T>::operator=(matrix const& other)
	{
		cols = new int(*other.cols);
		rows = new int(*other.rows);

		array = new T*[*cols];
		
		for(int i = 0; i < *cols; i++)
		{
			array[i] = new T[*rows];
		}

		for(int i = 0; i < *rows; i++)
		{
			for(int j = 0; j < *cols; j++){
				array[i][j] = other.array[i][j];
			}
		}

		name = new std::string(*(other.name));
		prev = other.prev;

		return (*this);
	}

	template <class T>
	std::ostream& operator <<(std::ostream & os, matrix<T> const& matrix)
	{

		os << "matrix: " << *matrix.name << std::endl;
		
		for (int i = 0; i < *(matrix.rows); i++)
		{
			for (int j = 0; j < *(matrix.cols); j++)
			{
				os << matrix.array[i][j] << " ";
			}

			os << std::endl;
		}

		os << std::endl << matrix.prev << std::endl;
		
		return os;
	}

}
