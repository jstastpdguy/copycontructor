#pragma once
#include <iostream>

namespace homework{

	template <class T>
	class matrix;

	template<class T>
	std::ostream& operator<< (std::ostream &, matrix<T> const&);
	
	template <class T>
	class matrix
	{
		T ** array;
		std::string * name;
		matrix<T>* prev;
		int* cols;
		int* rows;
	
	public:
	
		matrix() : array(nullptr), name(nullptr), prev(nullptr), cols(nullptr), rows(nullptr) {};
		matrix(int *, int *, std::string *, matrix<T> * = nullptr);
		matrix(matrix const&);
		~matrix();

		matrix& operator = (matrix const&);
		friend std::ostream& operator << <T>(std::ostream &, matrix<T> const&);
	};
}
